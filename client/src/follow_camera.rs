use bevy::prelude::*;

pub struct FollowCamera {
    pub camera_entity: Option<Entity>,
    pub target_entity: Option<Entity>,
}

impl FollowCamera {
    pub fn new() -> Self {
        Self {
            camera_entity: None,
            target_entity: None,
        }
    }

    pub fn set_camera_entity(&mut self, entity: Entity) {
        self.camera_entity = Some(entity)
    }

    pub fn follow(&mut self, entity: Entity) {
        self.target_entity = Some(entity)
    }

    #[allow(dead_code)]
    pub fn stop_following(&mut self) {
        self.target_entity = None
    }
}

/// Sets the position of the selected follow-camera to that of the followed entity
pub fn update_follow_camera(
    follow_camera_target: Res<FollowCamera>,
    mut query: Query<&mut Transform>,
) {
    let target = follow_camera_target
        .target_entity
        .and_then(|id| query.get_mut(id).ok().map(|t| t.translation));

    let camera = follow_camera_target
        .camera_entity
        .and_then(|id| query.get_mut(id).ok());

    if let (Some(mut camera), Some(target)) = (camera, target) {
        camera.translation = target;
    }
}
