use bevy::{math::vec2, prelude::*};
use bevy_networking_turbulence::NetworkResource;
use shared::{network::ClientMessage, player::MoveIntention};

#[derive(Debug, PartialEq, Eq)]
pub struct LastMoveIntention(pub Option<MoveIntention>);

pub fn keyboard_input(
    keyboard_input: Res<Input<KeyCode>>,
    mut last_move_intention: ResMut<LastMoveIntention>,
    mut move_intention_events: EventWriter<MoveIntentionEvent>,
) -> anyhow::Result<()> {
    let as_direction = |pressed, direction| if pressed { direction } else { Vec2::ZERO };

    let left = keyboard_input.pressed(KeyCode::A);
    let right = keyboard_input.pressed(KeyCode::D);
    let up = keyboard_input.pressed(KeyCode::W);
    let down = keyboard_input.pressed(KeyCode::S);

    let direction = as_direction(left, vec2(-1.0, 0.0))
        + as_direction(right, vec2(1.0, 0.0))
        + as_direction(up, vec2(0.0, 1.0))
        + as_direction(down, vec2(0.0, -1.0));

    let direction = if direction.length() == 0.0 {
        None
    } else {
        Some(direction)
    };

    let move_intention = MoveIntention::new(direction)?;

    if Some(move_intention) != last_move_intention.0 {
        // Move intention changed
        move_intention_events.send(MoveIntentionEvent::new(move_intention))
    }

    *last_move_intention = LastMoveIntention(Some(move_intention));

    Ok(())
}

// Sends move intentions to the server
pub fn send_move_intention_message(
    mut move_intention_events: EventReader<MoveIntentionEvent>,
    mut net: ResMut<NetworkResource>,
) -> anyhow::Result<()> {
    for event in move_intention_events.iter() {
        if let Some((&handle, _)) = net.connections.iter().next() {
            net.send_message(
                handle,
                ClientMessage::new_move_intention(event.move_intention),
            )
            .map_err(|err| anyhow::anyhow!("Failed to send move intention to server: {:?}", err))?;
        }
    }

    Ok(())
}

pub struct MoveIntentionEvent {
    pub move_intention: MoveIntention,
}

impl MoveIntentionEvent {
    pub fn new(move_intention: MoveIntention) -> Self {
        Self { move_intention }
    }
}
