use std::net::{Ipv4Addr, SocketAddr};

use bevy::{core::FixedTimestep, log, prelude::*};
use bevy_networking_turbulence::{NetworkEvent, NetworkResource, NetworkingPlugin};
use follow_camera::{update_follow_camera, FollowCamera};
use input::{keyboard_input, send_move_intention_message, LastMoveIntention, MoveIntentionEvent};
use player::create_player_entity;
use shared::{
    ids::PlayerId,
    network::{
        build_network_channels, ClientMessage, CreatePlayerEntityMessage,
        DespawnPlayerEntityMessage, MovePlayerEntityMessage, ServerMessage,
    },
    player::PlayerEntity,
    tiles::TilemapWriteOrders,
    SERVER_PORT,
};
use tilemap_entities::{setup_tilemap, update_tilemap, TilemapEntities};

mod follow_camera;
mod input;
mod player;
mod tilemap_entities;

fn main() {
    let mut app = App::build();
    app.insert_resource(Msaa { samples: 4 })
        // Logger settings
        .insert_resource(bevy::log::LogSettings {
            level: bevy::log::Level::DEBUG,
            ..Default::default()
        })
        // Default set of Bevy plugins including logging, graphics, frame runner etc
        .add_plugins(DefaultPlugins)
        // The NetworkingPlugin
        .add_plugin(NetworkingPlugin {
            idle_timeout_ms: Some(10000),
            auto_heartbeat_ms: None, //Some(3000),
            ..Default::default()
        })
        .add_startup_system(setup_networking.system())
        .add_startup_system(connect_to_server.system())
        .add_startup_system(setup_camera.system())
        // Read our networking messages in the pre-update stage so that it's all done when the update happens
        .add_system_to_stage(CoreStage::PreUpdate, read_network_channels.system())
        .add_system(handle_network_events.system())
        .add_system(
            send_move_intention_message
                .system()
                .chain(error_handler.system()),
        )
        // Tiles
        .add_startup_system(setup_tilemap.system())
        .add_system(update_tilemap.system().chain(error_handler.system()))
        .insert_resource(TilemapEntities::new())
        .insert_resource(TilemapWriteOrders::new())
        // Follow camera
        .insert_resource(FollowCamera::new())
        .add_system(update_follow_camera.system())
        // Player stuff
        .insert_resource(LocalPlayerId::new())
        // Input
        .add_system(keyboard_input.system().chain(error_handler.system()))
        .insert_resource(LastMoveIntention(None))
        .add_event::<MoveIntentionEvent>();

    #[cfg(target_arch = "wasm32")]
    app.add_plugin(bevy_webgl2::WebGL2Plugin);
    app.add_stage_after(
        CoreStage::PostUpdate,
        "heart_beat",
        SystemStage::parallel()
            .with_run_criteria(FixedTimestep::steps_per_second(0.5))
            .with_system(send_heartbeat.system()),
    );

    app.run()
}

fn setup_camera(mut commands: Commands, mut follow_camera: ResMut<FollowCamera>) {
    let mut bundle = OrthographicCameraBundle::new_2d();
    bundle.orthographic_projection.scale = 1.0 / 2.0;
    bundle.transform.translation.x += 200.0;
    bundle.transform.translation.y += 100.0;
    let camera = commands.spawn_bundle(bundle).id();

    // This is the camera that we want to use as a follow camera
    follow_camera.set_camera_entity(camera);
}

/// Set up the structural networking configuration, i.e. the channels.
fn setup_networking(mut net: ResMut<NetworkResource>) {
    build_network_channels(&mut net);
}

// Connect to the server with hard-coded credentials for now.
fn connect_to_server(mut net: ResMut<NetworkResource>) {
    //let ip_address =
    //    bevy_networking_turbulence::find_my_ip_address().expect("can't find ip address");
    let ip_address = Ipv4Addr::new(127, 0, 0, 1).into();
    let socket_address = SocketAddr::new(ip_address, SERVER_PORT);
    log::info!("Connecting to server at {}:{}", ip_address, SERVER_PORT);
    net.connect(socket_address);
}

/// This system handles networking events like clients connecting/disconnecting
fn handle_network_events(
    mut net: ResMut<NetworkResource>,
    mut network_events: EventReader<NetworkEvent>,
) {
    for event in network_events.iter() {
        match event {
            NetworkEvent::Connected(handle) => match net.connections.get_mut(handle) {
                Some(connection) => {
                    log::info!(
                        "Successfully connected to server at {:?}. Will ask to join the game",
                        connection.remote_address()
                    );
                    net.send_message(*handle, ClientMessage::RequestJoin)
                        .expect("Could not send request to join");
                }
                None => panic!("Got packet for non-existing connection [{}]", handle),
            },
            NetworkEvent::Disconnected(_) => log::debug!("Disconnected"),
            NetworkEvent::Packet(_, _) => log::debug!("Packet"),
            NetworkEvent::Error(handle, err) => {
                log::error!("Error on connection {}: {:?}", handle, err)
            }
        }
    }
}

/// This system receives messages from the networking channels
#[allow(clippy::too_many_arguments)]
fn read_network_channels(
    mut net: ResMut<NetworkResource>,
    //mut server_ids: ResMut<ServerIds>,
    mut tile_orders: ResMut<TilemapWriteOrders>,
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    mut local_player_id: ResMut<LocalPlayerId>,
    mut follow_camera: ResMut<FollowCamera>,
    player_entity_query: Query<(Entity, &PlayerEntity)>,
    mut move_entity_query: Query<&mut Transform>,
) {
    //log::debug!("Going to loop through connections");
    for (_handle, connection) in net.connections.iter_mut() {
        let channels = connection.channels().unwrap();

        while let Some(server_message) = channels.recv::<ServerMessage>() {
            match server_message {
                ServerMessage::YouJoined(m) => {
                    log::info!(
                        "Successfully joined the game as player ID {:?}. Writing initial tiles",
                        m.id
                    );
                    // TODO: don't do in networking routine
                    tile_orders.apply_stream(m.tiles);
                    local_player_id.id = Some(m.id);
                }
                ServerMessage::TileUpdates(orders) => {
                    log::info!("Got tiles updated from the server. Applying");
                    // TODO: don't do in networking routine
                    tile_orders.apply_stream(orders.0);
                }
                ServerMessage::Heartbeat => { /* don't need to handle */ }
                ServerMessage::CreatePlayerEntity(CreatePlayerEntityMessage {
                    position,
                    owner,
                }) => {
                    // TODO: don't do in networking routine
                    let player_entity = create_player_entity(
                        owner,
                        position,
                        &mut commands,
                        &asset_server,
                        &mut materials,
                    );

                    // If we own this entity, make the camera follow
                    if Some(owner) == local_player_id.id {
                        follow_camera.follow(player_entity);

                        log::info!(
                            "Creating our player entity {:?}. Attaching camera",
                            player_entity
                        );
                    } else {
                        log::info!(
                            "Creating player entity {:?} of player id {:?}.",
                            player_entity,
                            owner
                        );
                    }
                }
                ServerMessage::DespawnPlayerEntity(DespawnPlayerEntityMessage { owner }) => {
                    if let Some(to_remove) =
                        PlayerEntity::find_with_owner(&owner, &player_entity_query)
                    {
                        commands.entity(to_remove).despawn();
                    }
                }
                ServerMessage::MovePlayerEntity(MovePlayerEntityMessage { position, owner }) => {
                    if let Some(to_move) =
                        PlayerEntity::find_with_owner(&owner, &player_entity_query)
                    {
                        if let Ok(mut transform) = move_entity_query.get_mut(to_move) {
                            transform.translation.x = position.x;
                            transform.translation.y = position.y;
                        } else {
                            log::error!("Got instructions to move entity {:?} but we don't have that entity",to_move);
                        };
                    }
                }
            }
        }
    }
}

fn send_heartbeat(mut net: ResMut<NetworkResource>) {
    if let Some(handle) = net.connections.keys().cloned().next() {
        //log::debug!("Sending heartbeat to server");
        net.send_message(handle, ClientMessage::Heartbeat)
            .expect("Could not send heartbeat");
    }
}

fn error_handler(In(result): In<anyhow::Result<()>>) {
    if let Err(err) = result {
        log::error!("Error in system: {:?}", err);
    }
}

// TODO: Move elsewhere
pub struct LocalPlayerId {
    pub id: Option<PlayerId>,
}

impl LocalPlayerId {
    pub fn new() -> Self {
        Self { id: None }
    }
}

impl Default for LocalPlayerId {
    fn default() -> Self {
        Self::new()
    }
}
