use bevy::{math::vec3, prelude::*};
use shared::{ids::PlayerId, player::PlayerEntity};

pub fn create_player_entity(
    owner: PlayerId,
    position: Vec2,
    commands: &mut Commands,
    asset_server: &AssetServer,
    materials: &mut Assets<ColorMaterial>,
) -> Entity {
    let texture_handle = asset_server.load("player.png");

    commands
        .spawn_bundle(SpriteBundle {
            material: materials.add(texture_handle.into()),
            transform: Transform::from_translation(vec3(position.x, position.y, 10.0)),
            ..Default::default()
        })
        // Makes the entity owned as a player avatar by the given player id
        .insert(PlayerEntity { owner })
        .id()
}
