use anyhow::anyhow;
use bevy::{
    math::{uvec2, vec3},
    prelude::*,
};

use shared::{
    grid,
    tiles::{Tile, TilemapWriteOrders},
    TILEMAP_SIZE, TILE_SIZE,
};

pub struct TilemapAtlas(pub Handle<TextureAtlas>);

pub struct TilemapEntities {
    pub grid: grid::Grid2d<Option<Entity>>,
}

impl TilemapEntities {
    pub fn new() -> Self {
        Self {
            grid: grid::Grid2d::with_size(uvec2(TILEMAP_SIZE, TILEMAP_SIZE), None),
        }
    }
}

pub fn setup_tilemap(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    let texture_handle = asset_server.load("tiles.png");
    let texture_atlas = TextureAtlas::from_grid(texture_handle, Vec2::new(16.0, 16.0), 5, 5);
    let texture_atlas_handle = texture_atlases.add(texture_atlas);

    commands.insert_resource(TilemapAtlas(texture_atlas_handle));
}

pub fn update_tilemap(
    mut query: Query<&mut TextureAtlasSprite>,
    mut commands: Commands,
    tilemap_atlas: Res<TilemapAtlas>,
    mut tilemap_entities: ResMut<TilemapEntities>,
    mut tilemap_orders: ResMut<TilemapWriteOrders>,
) -> anyhow::Result<()> {
    if !tilemap_orders.is_changed() && !tilemap_orders.is_added() {
        return Ok(());
    }

    let texture_atlas_handle = tilemap_atlas.0.clone();

    let orders = std::mem::take(&mut *tilemap_orders);

    for order in orders.0.operations {
        let position = order.position;
        let new_tile = order.tile.map(Tile::from);
        let old_tile = tilemap_entities.grid.get_mut(&position)?;

        let updated_tile_value = match (*old_tile, new_tile) {
            (None, None) => None,
            (None, Some(new_tile)) => {
                // Create a new tile entity
                let tile_size = Vec2::splat(TILE_SIZE as f32);
                let translation = position.as_f32() * tile_size;

                let transform =
                    Transform::from_translation(vec3(translation.x, translation.y, 0.0));

                Some(
                    commands
                        .spawn_bundle(SpriteSheetBundle {
                            texture_atlas: texture_atlas_handle.clone(),
                            transform,
                            sprite: TextureAtlasSprite::new(new_tile.sprite_index),
                            ..Default::default()
                        })
                        .id(),
                )
            }
            (Some(old_tile), None) => {
                // Delete the tile entity
                commands.entity(old_tile).despawn();
                None
            }
            (Some(old_tile), Some(new_tile)) => {
                // Update sprite index
                let mut sprite = query.get_mut(old_tile).map_err(|_| {
                    anyhow!(
                        "Tile entity {:?} cannot be modified because it is missing",
                        old_tile
                    )
                })?;
                sprite.index = new_tile.sprite_index;

                Some(old_tile)
            }
        };

        *old_tile = updated_tile_value;
    }

    Ok(())
}
