use std::net::{IpAddr, Ipv4Addr, SocketAddr};

use anyhow::anyhow;

use bevy::{
    app::ScheduleRunnerSettings,
    log,
    math::{vec3, Vec3Swizzles},
    prelude::*,
    utils::Duration,
};
use bevy_networking_turbulence::{NetworkEvent, NetworkResource, NetworkingPlugin};
use bevy_rapier2d::physics::{NoUserData, RapierConfiguration, RapierPhysicsPlugin};
use movement::{broadcast_entity_movement, update_movement};
use player::{player_entity_broadcaster, PlayerEntityCreatedEvent, PlayerEntityDespawnedEvent};
use player_system::system::PlayerSystem;
use room_system::system::RoomSystem;

use shared::{
    network::{build_network_channels, ClientMessage, ServerMessage},
    player::MoveIntention,
    tiles::{Tile, TileStream, TilemapWriteOrders, Tiles},
    SERVER_PORT, TILE_SIZE,
};

use shared::player::PlayerEntity;

use crate::physics::{add_player_physics_components, create_solid_tile_physics_components};

mod movement;
mod physics;
mod player;
mod player_system;
mod room;
mod room_system;

// This example only enables a minimal set of plugins required for bevy to run.
// You can also completely remove rendering / windowing Plugin code from bevy
// by making your import look like this in your Cargo.toml
//
// [dependencies]
// bevy = { version = "*", default-features = false }
// # replace "*" with the most recent version of bevy

fn main() {
    // this app loops forever at 60 fps
    App::build()
        // Run the server simulation at the given rate
        .insert_resource(ScheduleRunnerSettings::run_loop(Duration::from_secs_f64(
            1.0 / 60.0,
        )))
        // Logger settings
        .insert_resource(bevy::log::LogSettings {
            level: bevy::log::Level::DEBUG,
            ..Default::default()
        })
        // Enable logger
        .add_plugin(bevy::log::LogPlugin)
        // Minimal set of Bevy engine plugins since we don't want all the graphical stuff in a headless server
        .add_plugins(MinimalPlugins)
        // The NetworkingPlugin
        .add_plugin(NetworkingPlugin {
            idle_timeout_ms: Some(10000),
            auto_heartbeat_ms: None, // Some(3000),
            ..Default::default()
        })
        .add_startup_system(setup_networking.system())
        .add_startup_system(start_server_listening.system())
        .add_system(counter.system())
        .add_system(handle_network_events.system())
        .add_system(read_network_channels.system().chain(error_handler.system()))
        .add_system(update_tilemap.system().chain(error_handler.system()))
        .insert_resource(Tiles::new())
        .insert_resource(RoomSystem::new())
        .insert_resource(TilemapWriteOrders::new())
        // Physics
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        .add_startup_system(configure_physics.system())
        // Player stuff
        .insert_resource(PlayerSystem::new())
        .add_event::<PlayerEntityCreatedEvent>()
        .add_event::<PlayerEntityDespawnedEvent>()
        .add_system(player_entity_broadcaster.system())
        .add_system(broadcast_entity_movement.system())
        // Movement
        .add_system(update_movement.system())
        .run();
}

fn counter(mut state: Local<CounterState>) {
    if state.count % 60 == 0 {}
    state.count += 1;
}

#[derive(Default)]
struct CounterState {
    count: u32,
}

/// Configure physics engine
fn configure_physics(mut rapier_config: ResMut<RapierConfiguration>) {
    rapier_config.gravity = Vec2::ZERO.into();
    rapier_config.scale = 1.0;
}

/// Set up the structural networking configuration, i.e. the channels.
fn setup_networking(mut net: ResMut<NetworkResource>) {
    build_network_channels(&mut net);
}

/// Let the server listen with the hard-coded credentials for now.
fn start_server_listening(mut net: ResMut<NetworkResource>) {
    //let ip_address =
    //    bevy_networking_turbulence::find_my_ip_address().expect("can't find ip address");
    let ip_address = IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1));
    let server_address = SocketAddr::new(ip_address, SERVER_PORT);

    log::info!(
        "Starting server listening on {}:{}",
        ip_address,
        SERVER_PORT
    );
    net.listen(server_address, None, None);
}

/// This system receives messages from the networking channels
#[allow(clippy::too_many_arguments)]
fn read_network_channels(
    mut net: ResMut<NetworkResource>,
    mut commands: Commands,
    mut players: ResMut<PlayerSystem>,
    player_entity_query: Query<(Entity, &PlayerEntity)>,
    transform_player_entity_query: Query<(&Transform, &PlayerEntity)>,
    mut rooms: ResMut<RoomSystem>,
    tiles: Res<Tiles>,
    mut tile_orders: ResMut<TilemapWriteOrders>,
    mut player_entity_created_events: EventWriter<PlayerEntityCreatedEvent>,
    mut move_intention_query: Query<&mut MoveIntention>,
) -> anyhow::Result<()> {
    for (handle, connection) in net.connections.iter_mut() {
        let remote_address = connection.remote_address();

        let channels = connection.channels().unwrap();

        while let Some(message) = channels.recv::<ClientMessage>() {
            match message {
                ClientMessage::RequestJoin => {
                    log::info!("Client on connection {:?} wishes to join.", remote_address);

                    let player_id = players
                        .create_player(*handle, &mut rooms, &mut tile_orders)
                        .map_err(|err| {
                            anyhow!("Failed to create player, could not insert room: {:?}", err)
                        })?;
                    log::info!("Player created: {:?}", player_id);

                    //// Send current state
                    // Send current tilemap payload to client
                    let all_tiles = TileStream::new_with_tiles(&*tiles);

                    log::info!("Sending initial tile payload to {}", handle);
                    channels.send(ServerMessage::new_you_joined(player_id, all_tiles));

                    // Send current player entities
                    for (transform, player_entity) in transform_player_entity_query.iter() {
                        channels.send(ServerMessage::new_create_player_entity(
                            transform.translation.xy(),
                            player_entity.owner,
                        ));
                    }

                    // Create player entity in one of the rooms
                    let room = rooms.rooms_owned_by(&player_id).next().ok_or_else(|| {
                        anyhow::anyhow!("Failed to find a room of the newly created player")
                    })?;

                    let position = shared::tiles::tile_to_world(room.region().center());

                    let commands = commands.spawn_bundle((
                        Transform::from_translation(vec3(position.x, position.y, 10.0)),
                        PlayerEntity::new(player_id),
                        MoveIntention::new_empty(),
                    ));

                    add_player_physics_components(position, commands);

                    player_entity_created_events.send(PlayerEntityCreatedEvent {
                        owner: player_id,
                        position,
                    })
                }
                ClientMessage::Heartbeat => {
                    // But we can log
                    //log::debug!("Received heartbeat from {}", handle);
                }
                ClientMessage::MoveIntention(move_intention) => {
                    if let Some(entity) =
                        players
                            .find_player_with_connection(*handle)
                            .and_then(|player| {
                                PlayerEntity::find_with_owner(&player, &player_entity_query)
                            })
                    {
                        if let Ok(mut target_move_intention) = move_intention_query.get_mut(entity)
                        {
                            *target_move_intention = move_intention;
                        } else {
                            log::error!("Entity {:?} has no move intention", entity);
                        }
                    } else {
                        log::warn!(
                            "Got move intention from {} but that connection has no player",
                            handle
                        );
                    }
                }
            }
        }
    }

    Ok(())
}

/// This system handles networking events like clients connecting/disconnecting
#[allow(clippy::too_many_arguments)]
fn handle_network_events(
    mut net: ResMut<NetworkResource>,
    mut network_events: EventReader<NetworkEvent>,
    mut players: ResMut<PlayerSystem>,
    mut rooms: ResMut<RoomSystem>,
    mut tile_orders: ResMut<TilemapWriteOrders>,
    query: Query<(Entity, &PlayerEntity)>,
    mut commands: Commands,
    mut player_entity_despawned_events: EventWriter<PlayerEntityDespawnedEvent>,
) {
    for event in network_events.iter() {
        match event {
            NetworkEvent::Connected(handle) => match net.connections.get_mut(handle) {
                Some(connection) => match connection.remote_address() {
                    Some(remote_address) => {
                        log::debug!(
                            "Incoming connection on [{}] from [{}]",
                            handle,
                            remote_address
                        );
                    }
                    None => {
                        log::debug!("Someone unknown connected on [{}]", handle);
                    }
                },
                None => panic!("Got packet for non-existing connection [{}]", handle),
            },
            NetworkEvent::Disconnected(handle) => match net.connections.get_mut(handle) {
                Some(connection) => match connection.remote_address() {
                    Some(remote_address) => {
                        log::debug!("Disconnection on [{}] from [{}]", handle, remote_address);
                    }
                    None => {
                        log::debug!("Someone unknown disconnected on [{}]", handle);
                    }
                },
                None => log::debug!("Got disconnect for handle [{}]", handle),
            },
            NetworkEvent::Packet(_, _) => log::debug!("Packet"),
            NetworkEvent::Error(handle, err) => match err {
                bevy_networking_turbulence::NetworkError::MissedHeartbeat => {
                    log::info!("Player {} seems to not respond. Removing", *handle);
                    if let Some(player_id) = players.find_player_with_connection(*handle) {
                        players.remove_player(
                            &player_id,
                            &mut rooms,
                            &mut tile_orders,
                            &query,
                            &mut commands,
                            &mut player_entity_despawned_events,
                        );
                    }
                }
                bevy_networking_turbulence::NetworkError::Disconnected => {
                    log::info!("Player {} disconnected", *handle);
                    if let Some(player_id) = players.find_player_with_connection(*handle) {
                        players.remove_player(
                            &player_id,
                            &mut rooms,
                            &mut tile_orders,
                            &query,
                            &mut commands,
                            &mut player_entity_despawned_events,
                        );
                    }
                }
                err => {
                    log::error!("Error on connection {}: {:?}", handle, err)
                }
            },
        }
    }
}

pub fn update_tilemap(
    mut tiles: ResMut<Tiles>,
    mut tilemap_orders: ResMut<TilemapWriteOrders>,
    mut net: ResMut<NetworkResource>,
    mut commands: Commands,
) -> anyhow::Result<()> {
    if !tilemap_orders.is_changed() && !tilemap_orders.is_added() {
        return Ok(());
    }

    log::info!("Tiles changed. Will send to clients");

    let orders = std::mem::take(&mut *tilemap_orders);

    // Write tiles
    for order in &orders.0.operations {
        let position = order.position;
        let new_tile = order.tile;
        let old_tile = tiles.grid.get_mut(&position)?;

        let updated_tile_value = match (*old_tile, new_tile) {
            (None, None) => None,
            (None, Some(new_tile)) => {
                // Create a new tile entity
                let tile_size = Vec2::splat(TILE_SIZE as f32);
                let translation = position.as_f32() * tile_size;

                let transform =
                    Transform::from_translation(vec3(translation.x, translation.y, 0.0));

                let entity_commands =
                    commands.spawn_bundle((transform, GlobalTransform::default()));

                let is_solid = Tile::from(new_tile).solid;

                let entity_commands = if is_solid {
                    create_solid_tile_physics_components(translation, entity_commands)
                } else {
                    entity_commands
                };

                Some((new_tile, entity_commands.id()))
            }
            (Some(old_tile), None) => {
                // Delete the tile entity
                commands.entity(old_tile.1).despawn();
                None
            }
            (Some(old_tile), Some(new_tile)) => {
                // Despawn then create a new entity
                commands.entity(old_tile.1).despawn();

                let tile_size = Vec2::splat(TILE_SIZE as f32);
                let translation = position.as_f32() * tile_size;

                let transform =
                    Transform::from_translation(vec3(translation.x, translation.y, 0.0));

                let entity_commands =
                    commands.spawn_bundle((transform, GlobalTransform::default()));

                let is_solid = Tile::from(new_tile).solid;

                let entity_commands = if is_solid {
                    create_solid_tile_physics_components(translation, entity_commands)
                } else {
                    entity_commands
                };

                Some((new_tile, entity_commands.id()))
            }
        };

        *old_tile = updated_tile_value;
    }

    // Send the tile updates to the clients
    for handle in net.connections.keys().cloned().collect::<Vec<_>>() {
        log::debug!("Sent tiles to {}", handle);

        net.send_message(handle, ServerMessage::TileUpdates(orders.clone()))
            .unwrap_or_else(|_| panic!("Could not send tiles to client {}", handle));
    }

    Ok(())
}

fn error_handler(In(result): In<anyhow::Result<()>>) {
    if let Err(err) = result {
        log::error!("Error in system: {:?}", err);
    }
}
