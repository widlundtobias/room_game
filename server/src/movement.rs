use bevy::{math::Vec3Swizzles, prelude::*};
use bevy_networking_turbulence::NetworkResource;
use bevy_rapier2d::prelude::RigidBodyVelocity;
use shared::{
    network::ServerMessage,
    player::{MoveIntention, PlayerEntity},
};

const PLAYER_SPEED: f32 = 50.0;

pub fn update_movement(mut query: Query<(&mut RigidBodyVelocity, &MoveIntention)>) {
    for (mut velocity, move_intention) in query.iter_mut() {
        let direction = move_intention.direction().unwrap_or_default();

        velocity.linvel = (direction * PLAYER_SPEED).into();
    }
}

// prints events as they come in
pub fn broadcast_entity_movement(
    query: Query<(&Transform, &PlayerEntity), Changed<Transform>>,
    mut net: ResMut<NetworkResource>,
) {
    for (transform, player_entity) in query.iter() {
        net.broadcast_message(ServerMessage::new_move_player_entity(
            transform.translation.xy(),
            player_entity.owner,
        ));
    }
}
