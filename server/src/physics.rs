use bevy::{ecs::system::EntityCommands, prelude::*};
use bevy_rapier2d::prelude::*;
use shared::TILE_SIZE;

use crate::player::player_hitbox_size;

pub fn add_player_physics_components<'a, 'b>(
    position: Vec2,
    mut commands: EntityCommands<'a, 'b>,
) -> EntityCommands<'a, 'b> {
    let half_size = player_hitbox_size() / 2.0;

    commands.insert_bundle(ColliderBundle {
        shape: ColliderShape::cuboid(half_size.x, half_size.y),
        material: ColliderMaterial::new(0.0, 0.0),
        position: position.into(),

        ..Default::default()
    });
    commands.insert_bundle(RigidBodyBundle::default());
    commands.insert(ColliderPositionSync::Discrete);

    commands
}

pub fn create_solid_tile_physics_components<'a, 'b>(
    position: Vec2,
    mut commands: EntityCommands<'a, 'b>,
) -> EntityCommands<'a, 'b> {
    let half_tile_size = (TILE_SIZE / 2) as f32;

    commands.insert_bundle(ColliderBundle {
        shape: ColliderShape::cuboid(half_tile_size, half_tile_size),
        material: ColliderMaterial::new(0.0, 0.0),
        position: position.into(),
        ..Default::default()
    });
    commands.insert_bundle(RigidBodyBundle {
        body_type: RigidBodyType::Static,
        ..Default::default()
    });
    commands.insert(ColliderPositionSync::Discrete);

    commands
}
