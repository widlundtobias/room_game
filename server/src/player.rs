use bevy::{
    log,
    math::{vec2, Vec2},
    prelude::*,
};
use bevy_networking_turbulence::NetworkResource;
use shared::{ids::PlayerId, network::ServerMessage};

pub fn player_hitbox_size() -> Vec2 {
    vec2(12.0, 6.0)
}

pub struct Player {
    pub connection_handle: u32,
}

impl Player {
    pub fn new(connection_handle: u32) -> Self {
        Self { connection_handle }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct PlayerEntityCreatedEvent {
    pub owner: PlayerId,
    pub position: Vec2,
}

impl From<PlayerEntityCreatedEvent> for ServerMessage {
    fn from(e: PlayerEntityCreatedEvent) -> Self {
        Self::new_create_player_entity(e.position, e.owner)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct PlayerEntityDespawnedEvent {
    pub owner: PlayerId,
}

impl From<PlayerEntityDespawnedEvent> for ServerMessage {
    fn from(e: PlayerEntityDespawnedEvent) -> Self {
        Self::new_despawn_player_entity(e.owner)
    }
}

// prints events as they come in
pub fn player_entity_broadcaster(
    mut created_events: EventReader<PlayerEntityCreatedEvent>,
    mut despawned_events: EventReader<PlayerEntityDespawnedEvent>,
    mut net: ResMut<NetworkResource>,
) {
    for event in created_events.iter() {
        log::info!(
            "Broadcasting the creation of player entity belonging to {:?}",
            event.owner
        );
        net.broadcast_message(ServerMessage::from(*event));
    }
    for event in despawned_events.iter() {
        log::info!(
            "Broadcasting the despawning of player entity belonging to {:?}",
            event.owner
        );
        net.broadcast_message(ServerMessage::from(*event));
    }
}
