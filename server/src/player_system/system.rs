use std::convert::TryFrom;

use bevy::{log, prelude::*, utils::HashMap};
use shared::{
    ids::PlayerId,
    player::PlayerEntity,
    tiles::{SetTileOperation, TileStream, TilemapWriteOrders},
};

use crate::{
    player::{Player, PlayerEntityDespawnedEvent},
    room::create_standard_room,
    room_system::system::{RoomInsertionError, RoomSystem},
};

pub struct PlayerSystem {
    players: HashMap<PlayerId, Player>,
}

impl PlayerSystem {
    pub fn new() -> Self {
        Self {
            players: HashMap::default(),
        }
    }

    pub fn find_player_with_connection(&self, handle: u32) -> Option<PlayerId> {
        self.players.iter().find_map(|(id, player)| {
            if player.connection_handle == handle {
                Some(*id)
            } else {
                None
            }
        })
    }

    pub fn create_player(
        &mut self,
        connection_handle: u32,
        rooms: &mut RoomSystem,
        tile_orders: &mut TilemapWriteOrders,
    ) -> Result<PlayerId, RoomInsertionError> {
        // New player ID
        let player_id = PlayerId::new();
        log::info!("Creating new player {:?}", player_id);
        // Store player
        self.players
            .insert(player_id, Player::new(connection_handle));

        // Create our player's room.
        let room = create_standard_room();

        // Place it
        let room_id = rooms.insert_room(room)?;
        log::info!("Giving new player room {:?}", room_id);

        let room = rooms
            .room(&room_id)
            .expect("Bug: room doesn't exist even though we just added it");

        // Write it to the tilemap
        let stream = TileStream::try_from(room)
            .expect("Bug: this room has no position even though it was placed");

        tile_orders.apply_stream(stream);

        // Register ownerships
        rooms.register_room_ownership(&player_id, &room_id);

        Ok(player_id)
    }

    pub fn remove_player(
        &mut self,
        id: &PlayerId,
        rooms: &mut RoomSystem,
        tile_orders: &mut TilemapWriteOrders,
        query: &Query<(Entity, &PlayerEntity)>,
        commands: &mut Commands,
        player_entity_despawned_events: &mut EventWriter<PlayerEntityDespawnedEvent>,
    ) {
        // Remove player struct
        self.players.remove(id);

        // Remove owned rooms
        if let Some(removed) = rooms.remove_all_owned_by(id) {
            // Write tiles into empty space again
            for to_remove in removed {
                let region = to_remove.region();

                let stream = TileStream::new_with_operations(
                    region.iter_coords().map(SetTileOperation::new_none),
                );
                tile_orders.apply_stream(stream);
            }
        }

        if let Some(to_remove) = PlayerEntity::find_with_owner(id, query) {
            player_entity_despawned_events.send(PlayerEntityDespawnedEvent { owner: *id });
            commands.entity(to_remove).despawn();
        }
    }
}
