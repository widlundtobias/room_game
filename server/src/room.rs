use bevy::{
    math::{uvec2, UVec2},
    utils::HashMap,
};

use shared::{
    orientation::Orientation,
    region::Region,
    tiles::{SetTileOperation, TileStream, TileTemplate},
};
use thiserror::Error;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct RoomId(pub uuid::Uuid);

impl RoomId {
    pub fn new() -> Self {
        Self(uuid::Uuid::new_v4())
    }
}

/// Type-based state of the Room
pub trait State: std::fmt::Debug {}

/// A non-placed room has not yet been placed in the world
#[derive(Debug)]
pub struct NonPlaced {}
impl State for NonPlaced {}

/// A room is placed when it has a location in the world
#[derive(Debug)]
pub struct Placed {
    pub start: UVec2,
}
impl State for Placed {}

/// A room that used to be placed in the world but is no longer
#[derive(Debug)]
pub struct WasPlaced {
    // Still carries the position it used to have
    pub start: UVec2,
}
impl State for WasPlaced {}

#[derive(Debug)]
pub struct Room<T: State> {
    pub size: UVec2,
    pub entrances: HashMap<EntranceId, Entrance>,
    state: T,
}

impl<T: State> Room<T> {
    pub fn entrance(&self, entrance_id: &EntranceId) -> Result<&Entrance, NoSuchEntranceError> {
        self.entrances.get(entrance_id).ok_or(NoSuchEntranceError {
            entrance_id: *entrance_id,
        })
    }
}

impl Room<NonPlaced> {
    pub fn place(self, start: UVec2) -> Room<Placed> {
        Room {
            size: self.size,
            entrances: self.entrances,
            state: Placed { start },
        }
    }
}

impl Room<Placed> {
    pub fn start(&self) -> UVec2 {
        self.state.start
    }

    pub fn entrance_world_pos(
        &self,
        entrance_id: &EntranceId,
    ) -> Result<UVec2, RoomEntrancePositionError> {
        let entrance = self.entrance(entrance_id)?;

        Ok(self.start() + entrance.position)
    }

    pub fn region(&self) -> Region {
        Region::with_start_size(self.start(), self.size)
    }

    pub fn unplace(self) -> Room<WasPlaced> {
        Room {
            size: self.size,
            entrances: self.entrances,
            state: WasPlaced {
                start: self.state.start,
            },
        }
    }
}

impl Room<WasPlaced> {
    pub fn start(&self) -> UVec2 {
        self.state.start
    }

    pub fn entrance_world_pos(
        &self,
        entrance_id: &EntranceId,
    ) -> Result<UVec2, RoomEntrancePositionError> {
        let entrance = self.entrance(entrance_id)?;

        Ok(self.start() + entrance.position)
    }

    pub fn region(&self) -> Region {
        Region::with_start_size(self.start(), self.size)
    }
}

impl From<&Room<Placed>> for TileStream {
    fn from(room: &Room<Placed>) -> Self {
        let region = room.region();

        let mut result = Self::new();

        let is_wall = |position: &UVec2| {
            position.x == 0
                || position.y == 0
                || position.x == room.size.x - 1
                || position.y == room.size.y - 1
        };
        let is_door = |position: &UVec2| room.entrances.values().any(|e| &e.position == position);

        for position in region.iter_coords() {
            let local_pos = position - room.start();

            let is_wall = is_wall(&local_pos);
            let is_door = is_door(&local_pos);
            let tile = match (is_wall, is_door) {
                (_, true) => TileTemplate::ClosedDoor,
                (true, false) => TileTemplate::Wall,
                (false, false) => TileTemplate::Floor,
            };

            result.operations.push(SetTileOperation {
                position,
                tile: Some(tile),
            });
        }

        result
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct EntranceId(pub uuid::Uuid);

impl EntranceId {
    pub fn new() -> Self {
        Self(uuid::Uuid::new_v4())
    }
}

impl Default for EntranceId {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug)]
pub struct Entrance {
    pub position: UVec2,
    pub orientation: Orientation,
}

impl Entrance {
    pub fn new_n(position: UVec2) -> Self {
        Self {
            position,
            orientation: Orientation::North,
        }
    }

    pub fn new_e(position: UVec2) -> Self {
        Self {
            position,
            orientation: Orientation::East,
        }
    }

    pub fn new_s(position: UVec2) -> Self {
        Self {
            position,
            orientation: Orientation::South,
        }
    }

    pub fn new_w(position: UVec2) -> Self {
        Self {
            position,
            orientation: Orientation::West,
        }
    }
}

impl Entrance {
    pub fn new(position: UVec2, orientation: Orientation) -> Self {
        Self {
            position,
            orientation,
        }
    }
}

#[derive(Debug, Error)]
#[error("entrance ID {entrance_id:?} does not exist")]
pub struct NoSuchEntranceError {
    pub entrance_id: EntranceId,
}

#[derive(Debug, Error)]
#[error("{0}")]
pub enum RoomEntrancePositionError {
    NoSuchEntrance(#[from] NoSuchEntranceError),
}

pub fn create_standard_room() -> Room<NonPlaced> {
    let size = uvec2(5, 5);

    let x_mid = size.x / 2;
    let y_mid = size.y / 2;

    let x_last = size.x - 1;
    let y_last = size.y - 1;

    Room {
        size,
        entrances: std::array::IntoIter::new([
            (EntranceId::new(), Entrance::new_n(uvec2(x_mid, 0))),
            (EntranceId::new(), Entrance::new_e(uvec2(x_last, y_mid))),
            (EntranceId::new(), Entrance::new_s(uvec2(x_mid, y_last))),
            (EntranceId::new(), Entrance::new_w(uvec2(0, y_mid))),
        ])
        .collect(),
        state: NonPlaced {},
    }
}
