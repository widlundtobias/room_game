use bevy::utils::HashSet;

use shared::orientation::Orientation;
use thiserror::Error;

use crate::room::{EntranceId, NoSuchEntranceError, NonPlaced, Placed, Room, RoomId};

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub(super) struct Connectable {
    pub(super) entrance_id: EntranceId,
    pub(super) orientation: Orientation,
    pub(super) room: RoomId,
}

#[derive(Debug, Clone, Copy)]
struct Connection {
    a: Connectable,
    b: Connectable,
}

impl Connection {
    fn involves(&self, entrance: &EntranceId) -> bool {
        &self.a.entrance_id == entrance || &self.b.entrance_id == entrance
    }

    fn disconnect_from(self, entrance: &EntranceId) -> Result<Connectable, NoSuchEntranceError> {
        if self.involves(entrance) {
            let disconnected = if entrance == &self.a.entrance_id {
                self.b
            } else {
                self.a
            };

            Ok(disconnected)
        } else {
            Err(NoSuchEntranceError {
                entrance_id: *entrance,
            })
        }
    }
}

pub struct ConnectivityTracker {
    open_connectables: HashSet<Connectable>,
    connections: Vec<Connection>,
}

impl ConnectivityTracker {
    pub fn new() -> ConnectivityTracker {
        ConnectivityTracker {
            open_connectables: HashSet::default(),
            connections: Vec::new(),
        }
    }

    pub(super) fn connectable_candidates(
        &self,
        room: &Room<NonPlaced>,
    ) -> Vec<(EntranceId, Vec<Connectable>)> {
        room.entrances
            .iter()
            .filter_map(|(id, entrance)| {
                let connectables: Vec<_> = self
                    .open_connectables
                    .iter()
                    .filter(|connectable| {
                        entrance.orientation.is_opposite_of(connectable.orientation)
                    })
                    .cloned()
                    .collect();

                if connectables.is_empty() {
                    None
                } else {
                    Some((*id, connectables))
                }
            })
            .collect()
    }

    pub(super) fn connect_room_and_add_remaining_entrances(
        &mut self,
        room_id: &RoomId,
        room: &Room<Placed>,
        entrance_id: &EntranceId,
        target_connectable: &Connectable,
    ) -> Result<(), RoomConnectionError> {
        //// Connect the given entrance to the target_connectable
        if !self.open_connectables.remove(target_connectable) {
            return Err(NoSuchConnectableError {
                connectable: *target_connectable,
            }
            .into());
        }

        let entrance = room.entrance(entrance_id)?;

        self.connections.push(Connection {
            a: Connectable {
                entrance_id: *entrance_id,
                orientation: entrance.orientation,
                room: *room_id,
            },
            b: *target_connectable,
        });

        //// Add remaining entrances as open connectables
        for (to_add_id, to_add) in room.entrances.iter().filter(|(&e, _)| e != *entrance_id) {
            let connectable = Connectable {
                entrance_id: *to_add_id,
                orientation: to_add.orientation,
                room: *room_id,
            };

            let world_pos = room
                .entrance_world_pos(to_add_id)
                .expect("Bug: Accessing entrance in room that's looping. Entrance should exist");

            if world_pos.x > 0 && world_pos.y > 0 {
                let existed = !self.open_connectables.insert(connectable);

                if existed {
                    return Err(OpenConnectableAlreadyExistsError { connectable }.into());
                }
            }
        }

        Ok(())
    }

    pub(super) fn add_room_entrances(
        &mut self,
        room_id: &RoomId,
        room: &Room<Placed>,
    ) -> Result<(), OpenConnectableAlreadyExistsError> {
        //// Add entrances as open connectables
        for (to_add_id, to_add) in room.entrances.iter() {
            let world_pos = room
                .entrance_world_pos(to_add_id)
                .expect("Bug: Accessing entrance in room that's looping. Entrance should exist");

            if world_pos.x > 0 && world_pos.y > 0 {
                // TODO: We check map borders here. Ideally map should not be bounded to positive numbers
                let connectable = Connectable {
                    entrance_id: *to_add_id,
                    orientation: to_add.orientation,
                    room: *room_id,
                };

                let existed = !self.open_connectables.insert(connectable);

                if existed {
                    return Err(OpenConnectableAlreadyExistsError { connectable });
                }
            }
        }

        Ok(())
    }

    pub fn remove_entrance(&mut self, entrance_id: &EntranceId) {
        // Remove the entrance from the open list if it's there
        self.open_connectables
            .retain(|c| &c.entrance_id != entrance_id);

        let mut new_open = Vec::new();

        // Remove any connection involving this entrance, and convert it into a connectable for the open list
        self.connections.retain(|connection| {
            if connection.involves(entrance_id) {
                let disconnected = connection.disconnect_from(entrance_id).unwrap(); // Unwrap safe since it's guarded by if statement
                new_open.push(disconnected);
                false
            } else {
                true
            }
        });

        self.open_connectables.extend(new_open.into_iter());
    }
}

#[derive(Debug, Error)]
#[error("connectable {connectable:?} does not exist")]
pub struct NoSuchConnectableError {
    connectable: Connectable,
}

#[derive(Debug, Error)]
#[error("the connectable {connectable:?} already exists")]
pub struct OpenConnectableAlreadyExistsError {
    connectable: Connectable,
}

#[derive(Debug, Error)]
#[error("{0}")]
pub enum RoomConnectionError {
    NoSuchConnectableError(#[from] NoSuchConnectableError),
    NoSuchEntrance(#[from] NoSuchEntranceError),
    OpenConnectableAlreadyExistsError(#[from] OpenConnectableAlreadyExistsError),
}
