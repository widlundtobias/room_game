use bevy::utils::{HashMap, HashSet};
use shared::ids::PlayerId;

use crate::room::RoomId;

pub struct OwnershipRegistry {
    players_own: HashMap<PlayerId, HashSet<RoomId>>,
    rooms_owned_by: HashMap<RoomId, PlayerId>,
}

impl OwnershipRegistry {
    pub fn new() -> Self {
        Self {
            players_own: HashMap::default(),
            rooms_owned_by: HashMap::default(),
        }
    }

    pub fn register_ownership(&mut self, player: &PlayerId, room: &RoomId) {
        // TODO: Verify that no one else owns this room
        match self.players_own.entry(*player) {
            std::collections::hash_map::Entry::Occupied(mut e) => {
                e.get_mut().insert(*room);
            }
            std::collections::hash_map::Entry::Vacant(e) => {
                let mut set = HashSet::default();
                set.insert(*room);
                e.insert(set);
            }
        }

        self.rooms_owned_by.insert(*room, *player);
    }

    pub fn rooms_owned_by(&self, player: &PlayerId) -> impl Iterator<Item = RoomId> {
        let rooms: Vec<_> = self
            .players_own
            .get(player)
            .map(|rooms| rooms.iter().cloned().collect())
            .unwrap_or_default();

        rooms.into_iter()
    }

    pub fn remove_all_owned_by(&mut self, player: &PlayerId) -> Option<Vec<RoomId>> {
        let removed = self.players_own.remove(player);

        removed.map(|rooms| {
            for room in &rooms {
                self.rooms_owned_by.remove(room);
            }
            rooms.into_iter().collect()
        })
    }
}
