use bevy::{log, math::uvec2, utils::HashMap};
use rand::prelude::SliceRandom;
use shared::{
    grid::{self, WriteOutOfBoundsError},
    ids::PlayerId,
    tiles::TileTemplate,
};
use thiserror::Error;

use crate::room::{
    EntranceId, NoSuchEntranceError, NonPlaced, Placed, Room, RoomEntrancePositionError, RoomId,
    WasPlaced,
};

use super::{
    connectivity_tracker::{
        Connectable, ConnectivityTracker, OpenConnectableAlreadyExistsError, RoomConnectionError,
    },
    ownership_registry::OwnershipRegistry,
};

pub struct RoomSystem {
    rooms: HashMap<RoomId, Room<Placed>>,
    ownership: OwnershipRegistry,
    connectivity: ConnectivityTracker,
}

impl RoomSystem {
    pub fn new() -> Self {
        Self {
            rooms: HashMap::default(),
            ownership: OwnershipRegistry::new(),
            connectivity: ConnectivityTracker::new(),
        }
    }

    pub fn room(&self, room_id: &RoomId) -> Result<&Room<Placed>, NoSuchRoomError> {
        self.rooms
            .get(room_id)
            .ok_or(NoSuchRoomError { room_id: *room_id })
    }

    pub fn insert_room(&mut self, room: Room<NonPlaced>) -> Result<RoomId, RoomInsertionError> {
        let new_id = if self.rooms.is_empty() {
            //// We have the genesis room, so we just insert it and add all entrances as connectables

            ////
            //  Make the position of the room something decent
            let position = uvec2(0, 0);
            let room = room.place(position);

            log::info!("Creating genesis room at {}", position);

            ////
            //  Generate the new ID of our room
            let new_id = RoomId::new();

            ////
            //  Add the room entrances as connectables
            self.connectivity.add_room_entrances(&new_id, &room)?;

            ////
            //  Store the room
            self.rooms.insert(new_id, room); // new_id can technically collide but we trust UUIDs

            new_id
        } else {
            ////
            // First we need to find a place in the world to place this room.
            // TODO: Some things need to be taken into account that can block where a room goes:
            // - Tilemap (I think anyway)
            // - Blocking entrances of other rooms
            // - Overlapping with other rooms
            //
            // But for now, just find a suitable connectable to connect it to
            let (entrance_id, suggested_connectable) =
                self.find_suitable_room_connectable(&room)
                    .ok_or(RoomInsertionError::NoConnectionFound)?;

            log::info!(
                "Found suitable connection for new room on {:?}",
                suggested_connectable
            );

            ////
            //  Now we need to adjust the room we are connecting so that its position is changed
            //  So that it slots up against the target connectable.
            //
            //  Find the room of the connectable we are connecting to and use it to find out the
            //  world position of the connectable. The formula for where to place our room:
            //
            //  pos = target_connectable_pos - entrance_to_connect_pos - orientation_adjust
            let room_to_connect_to = self.room(&suggested_connectable.room)?;
            let target_entrance_world_pos =
                room_to_connect_to.entrance_world_pos(&suggested_connectable.entrance_id)?;
            let entrance_to_connect = room.entrance(&entrance_id)?;

            let room_position = (target_entrance_world_pos.as_i32()
                - entrance_to_connect.position.as_i32()
                - entrance_to_connect.orientation.direction_vector_i())
            .as_u32();

            log::info!("Creating room at {}", room_position);

            let room = room.place(room_position);

            ////
            //  Generate the new ID of our room
            let new_id = RoomId::new();

            ////
            //  Register the connection. Also adds the remaining entrances as connectables
            self.connectivity.connect_room_and_add_remaining_entrances(
                &new_id,
                &room,
                &entrance_id,
                &suggested_connectable,
            )?;

            ////
            //  Store the room
            self.rooms.insert(new_id, room); // new_id can technically collide but we trust UUIDs

            new_id
        };

        log::info!("Created room ID: {:?}", new_id);

        ////
        //  Return the new room's ID
        Ok(new_id)
    }

    pub fn rooms_owned_by(&self, player: &PlayerId) -> impl Iterator<Item = &Room<Placed>> {
        self.ownership
            .rooms_owned_by(player)
            .filter_map(move |id| self.rooms.get(&id))
    }

    pub fn remove_all_owned_by(&mut self, player: &PlayerId) -> Option<Vec<Room<WasPlaced>>> {
        let ids = self.ownership.remove_all_owned_by(player);

        if let Some(ids) = ids {
            let removed = ids.into_iter().filter_map(|id| {
                if let Some(room) = self.rooms.remove(&id) {
                    // Disconnect the removed room and remove it from available connectivity
                    for entrance in room.entrances.keys() {
                        self.connectivity.remove_entrance(entrance);
                    }

                    Some(room.unplace())
                } else {
                    None
                }
            });

            Some(removed.collect())
        } else {
            None
        }
    }

    pub fn register_room_ownership(&mut self, player: &PlayerId, room: &RoomId) {
        self.ownership.register_ownership(player, room);
    }

    fn find_suitable_room_connectable(
        &self,
        room: &Room<NonPlaced>,
    ) -> Option<(EntranceId, Connectable)> {
        let candidates = self.connectivity.connectable_candidates(room);

        let (entrance, candidates) = candidates.choose(&mut rand::thread_rng())?;
        let connectable = candidates.choose(&mut rand::thread_rng())?;

        Some((*entrance, *connectable))
    }
}

#[derive(Debug, Error)]
#[error("room ID {room_id:?} does not exist")]
pub struct NoSuchRoomError {
    room_id: RoomId,
}

#[derive(Debug, Error)]
#[error("{0}")]
pub enum RoomTileError {
    NoSuchRoom(#[from] NoSuchRoomError),
    OutOfBounds(#[from] grid::ReadOutOfBoundsError),
}

#[derive(Debug, Error)]
#[error("{0}")]
pub enum RoomInsertionError {
    NoSuchRoom(#[from] NoSuchRoomError),
    NoSuchEntrance(#[from] NoSuchEntranceError),
    RoomEntrancePosition(#[from] RoomEntrancePositionError),
    OutOfBounds(#[from] grid::ReadOutOfBoundsError),
    #[error("no suitable connection could be found")]
    NoConnectionFound,
    RoomConnection(#[from] RoomConnectionError),
    OpenConnectableAlreadyExists(#[from] OpenConnectableAlreadyExistsError),
    WriteOutOfBounds(#[from] WriteOutOfBoundsError<Option<TileTemplate>>),
}
