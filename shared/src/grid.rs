use bevy::math::{uvec2, UVec2};
use thiserror::Error;

use crate::region::Region;

pub struct Grid2d<T: Clone> {
    size: UVec2,
    cells: Vec<T>,
}

#[derive(Error, Debug)]
#[error("reading grid of size {size} at {position}")]
pub struct ReadOutOfBoundsError {
    pub position: UVec2,
    pub size: UVec2,
}

#[derive(Debug, Error)]
#[error("writing grid of size {size} at {position}")]
pub struct WriteOutOfBoundsError<T> {
    pub position: UVec2,
    pub size: UVec2,
    pub rejected: T,
}

impl<T: Clone> Grid2d<T> {
    pub fn with_size(size: UVec2, value: T) -> Self {
        let total_cells = (size.x * size.y) as usize;
        let mut cells = Vec::with_capacity(total_cells);
        cells.resize(total_cells, value);

        Self { size, cells }
    }

    pub fn size(&self) -> UVec2 {
        self.size
    }

    pub fn contains(&self, pos: &UVec2) -> bool {
        (0..self.size.x).contains(&pos.x) && (0..self.size.y).contains(&pos.y)
    }

    /// Set a value in the grid.
    pub fn set(&mut self, pos: &UVec2, value: T) -> Result<(), WriteOutOfBoundsError<T>> {
        if self.contains(pos) {
            let index = self.to_index(pos);
            self.cells[index] = value;

            Ok(())
        } else {
            Err(WriteOutOfBoundsError {
                rejected: value,
                position: *pos,
                size: self.size,
            })
        }
    }

    pub fn get(&self, pos: &UVec2) -> Result<&T, ReadOutOfBoundsError> {
        if self.contains(pos) {
            let index = self.to_index(pos);
            Ok(&self.cells[index])
        } else {
            Err(ReadOutOfBoundsError {
                position: *pos,
                size: self.size,
            })
        }
    }

    pub fn get_mut(&mut self, pos: &UVec2) -> Result<&mut T, ReadOutOfBoundsError> {
        if self.contains(pos) {
            let index = self.to_index(pos);
            Ok(&mut self.cells[index])
        } else {
            Err(ReadOutOfBoundsError {
                position: *pos,
                size: self.size,
            })
        }
    }

    fn to_index(&self, pos: &UVec2) -> usize {
        (pos.x as usize) + (pos.y * self.size.x) as usize
    }

    pub fn iter_coords(&self) -> impl Iterator<Item = UVec2> {
        Region::with_start_size(uvec2(0, 0), self.size).iter_coords()
    }

    pub fn map_to_tiles<'a, 'b>(
        &'b self,
        coords: impl IntoIterator<Item = &'a UVec2>,
    ) -> Result<impl Iterator<Item = (UVec2, &'b T)>, ReadOutOfBoundsError> {
        let tiles = coords
            .into_iter()
            .map(|c| self.get(c).map(|tile| (*c, tile)));

        let v: Result<Vec<(UVec2, &T)>, ReadOutOfBoundsError> = tiles.collect();

        Ok(v?.into_iter())
    }
}
