pub mod grid;
pub mod ids;
pub mod network;
pub mod orientation;
pub mod player;
pub mod region;
pub mod tiles;

pub const SERVER_PORT: u16 = 9002;

pub const TILEMAP_SIZE: u32 = 20;
pub const TILE_SIZE: u32 = 16;
