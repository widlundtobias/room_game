use std::time::Duration;

use bevy::math::Vec2;
use serde::{Deserialize, Serialize};

use bevy_networking_turbulence::{
    ConnectionChannelsBuilder, MessageChannelMode, MessageChannelSettings, NetworkResource,
    ReliableChannelSettings,
};

use crate::{
    ids::PlayerId,
    player::MoveIntention,
    tiles::{TileStream, TilemapWriteOrders},
};

pub fn build_network_channels(net: &mut NetworkResource) {
    // TODO: Move this to shared
    net.set_channels_builder(|builder: &mut ConnectionChannelsBuilder| {
        builder
            .register::<ServerMessage>(SERVER_MESSAGE_SETTINGS)
            .expect("Failed to register network message");
        builder
            .register::<ClientMessage>(CLIENT_MESSAGE_SETTINGS)
            .expect("Failed to register network message");
    });
}

// Channels are always full-duplex, meaning they allow for two-way simultaneous communication between the connected parties

pub const CLIENT_MESSAGE_SETTINGS: MessageChannelSettings = MessageChannelSettings {
    channel: 0,
    channel_mode: MessageChannelMode::Reliable {
        reliability_settings: ReliableChannelSettings {
            bandwidth: 4096,
            recv_window_size: 1024,
            send_window_size: 1024,
            burst_bandwidth: 1024,
            init_send: 512,
            wakeup_time: Duration::from_millis(100),
            initial_rtt: Duration::from_millis(200),
            max_rtt: Duration::from_secs(2),
            rtt_update_factor: 0.1,
            rtt_resend_factor: 1.5,
        },
        max_message_len: 1024,
    },
    message_buffer_size: 8,
    packet_buffer_size: 8,
};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum ClientMessage {
    /// Sent by clients who wish to join the game
    RequestJoin,
    Heartbeat,
    MoveIntention(MoveIntention),
}

impl ClientMessage {
    pub fn new_move_intention(move_intention: MoveIntention) -> Self {
        Self::MoveIntention(move_intention)
    }
}

pub const SERVER_MESSAGE_SETTINGS: MessageChannelSettings = MessageChannelSettings {
    channel: 1,
    channel_mode: MessageChannelMode::Reliable {
        reliability_settings: ReliableChannelSettings {
            bandwidth: 4096,
            recv_window_size: 1024,
            send_window_size: 1024,
            burst_bandwidth: 1024,
            init_send: 512,
            wakeup_time: Duration::from_millis(100),
            initial_rtt: Duration::from_millis(200),
            max_rtt: Duration::from_secs(2),
            rtt_update_factor: 0.1,
            rtt_resend_factor: 1.5,
        },
        max_message_len: 8192 * 2,
    },
    message_buffer_size: 8,
    packet_buffer_size: 8,
};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum ServerMessage {
    /// Sent to a newly joined player
    YouJoined(YouJoinedMessage),
    TileUpdates(TilemapWriteOrders),
    Heartbeat,
    CreatePlayerEntity(CreatePlayerEntityMessage),
    DespawnPlayerEntity(DespawnPlayerEntityMessage),
    MovePlayerEntity(MovePlayerEntityMessage),
}

impl ServerMessage {
    pub fn new_you_joined(id: PlayerId, tiles: TileStream) -> Self {
        Self::YouJoined(YouJoinedMessage { id, tiles })
    }

    pub fn new_create_player_entity(position: Vec2, owner: PlayerId) -> Self {
        Self::CreatePlayerEntity(CreatePlayerEntityMessage { position, owner })
    }

    pub fn new_despawn_player_entity(owner: PlayerId) -> Self {
        Self::DespawnPlayerEntity(DespawnPlayerEntityMessage { owner })
    }

    pub fn new_move_player_entity(position: Vec2, owner: PlayerId) -> Self {
        Self::MovePlayerEntity(MovePlayerEntityMessage { position, owner })
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct YouJoinedMessage {
    pub id: PlayerId,
    pub tiles: TileStream,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CreatePlayerEntityMessage {
    pub position: Vec2,
    pub owner: PlayerId,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DespawnPlayerEntityMessage {
    pub owner: PlayerId,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MovePlayerEntityMessage {
    pub position: Vec2,
    pub owner: PlayerId,
}
