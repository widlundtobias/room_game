use bevy::math::{ivec2, IVec2};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Orientation {
    North,
    East,
    South,
    West,
}

impl Orientation {
    pub fn is_opposite_of(&self, other: Orientation) -> bool {
        match self {
            Orientation::North => other == Orientation::South,
            Orientation::East => other == Orientation::West,
            Orientation::South => other == Orientation::North,
            Orientation::West => other == Orientation::East,
        }
    }

    pub fn direction_vector_i(&self) -> IVec2 {
        match self {
            Orientation::North => ivec2(0, -1),
            Orientation::East => ivec2(1, 0),
            Orientation::South => ivec2(0, 1),
            Orientation::West => ivec2(-1, 0),
        }
    }
}
