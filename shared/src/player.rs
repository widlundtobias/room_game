use bevy::{math::Vec2, prelude::*};
use serde::{Deserialize, Serialize};
use thiserror::Error;

use crate::ids::PlayerId;

/// Entities with this component are player-controlled avatars
pub struct PlayerEntity {
    pub owner: PlayerId,
}

impl PlayerEntity {
    pub fn new(owner: PlayerId) -> Self {
        Self { owner }
    }

    pub fn find_with_owner(
        owner: &PlayerId,
        query: &Query<(Entity, &PlayerEntity)>,
    ) -> Option<Entity> {
        query.iter().find_map(|(entity, player_entity)| {
            if &player_entity.owner == owner {
                Some(entity)
            } else {
                None
            }
        })
    }
}

#[derive(Debug, PartialEq, Clone, Copy, Serialize, Deserialize)]
pub struct MoveIntention {
    direction: Option<Vec2>,
}

impl Eq for MoveIntention {}

impl MoveIntention {
    pub fn new(direction: Option<Vec2>) -> Result<Self, ZeroVectorError> {
        if let Some(direction) = direction {
            if direction.length() == 0.0 {
                return Err(ZeroVectorError {});
            }
        }

        Ok(Self {
            direction: direction.map(|v| v.normalize()),
        })
    }

    pub fn new_empty() -> Self {
        Self { direction: None }
    }

    pub fn direction(&self) -> &Option<Vec2> {
        &self.direction
    }
}

#[derive(Debug, Error)]
#[error("the given vector is zero length")]
pub struct ZeroVectorError {}
