use bevy::math::{uvec2, UVec2, Vec2};

#[derive(Debug, Clone, Copy)]
pub struct Region {
    pub start: UVec2,
    pub size: UVec2,
}

impl Region {
    pub fn with_start_size(start: UVec2, size: UVec2) -> Self {
        Self { start, size }
    }

    pub fn iter_coords(&self) -> impl Iterator<Item = UVec2> {
        let start = self.start;
        let end = start + self.size;

        (start.y..end.y)
            .into_iter()
            .map(|y| (start.x..end.x).into_iter().map(move |x| uvec2(x, y)))
            .flatten()
            .collect::<Vec<_>>()
            .into_iter()
    }

    pub fn center(&self) -> UVec2 {
        self.start + self.size / 2
    }

    pub fn center_f32(&self) -> Vec2 {
        self.start.as_f32() + self.size.as_f32() / 2.0
    }
}
