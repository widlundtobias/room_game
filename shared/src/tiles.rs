use bevy::{
    math::{uvec2, vec2, UVec2, Vec2},
    prelude::Entity,
};

use serde::{Deserialize, Serialize};

use crate::{grid, region::Region, TILEMAP_SIZE, TILE_SIZE};

#[derive(Debug, Copy, Clone, Deserialize, Serialize)]
pub struct Tile {
    pub sprite_index: u32,
    pub solid: bool,
}

impl Tile {
    pub fn new(sprite_index: u32, solid: bool) -> Self {
        Self {
            sprite_index,
            solid,
        }
    }
}

pub struct Tiles {
    pub grid: grid::Grid2d<Option<(TileTemplate, Entity)>>,
}

impl Tiles {
    pub fn new() -> Self {
        Self {
            grid: grid::Grid2d::with_size(uvec2(TILEMAP_SIZE, TILEMAP_SIZE), None),
        }
    }
}

impl Default for Tiles {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub struct SetTileOperation {
    pub position: UVec2,
    pub tile: Option<TileTemplate>,
}

impl SetTileOperation {
    pub fn new_none(position: UVec2) -> Self {
        Self {
            position,
            tile: None,
        }
    }
}

impl From<(UVec2, Option<TileTemplate>)> for SetTileOperation {
    fn from((position, tile): (UVec2, Option<TileTemplate>)) -> Self {
        Self { position, tile }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TileStream {
    pub operations: Vec<SetTileOperation>,
}

impl TileStream {
    pub fn new() -> Self {
        Self {
            operations: Vec::new(),
        }
    }

    pub fn new_with_operations(operations: impl IntoIterator<Item = SetTileOperation>) -> Self {
        Self {
            operations: operations.into_iter().collect(),
        }
    }

    pub fn new_with_tiles(tiles: &Tiles) -> Self {
        let coords: Vec<_> = tiles.grid.iter_coords().collect();
        let tiles = tiles.grid.map_to_tiles(&coords).unwrap(); // Using coords retreived from the same map should never fail

        Self {
            operations: tiles
                .into_iter()
                .map(|(coord, entry)| {
                    let tile = entry.map(|(tile, _)| tile);
                    (coord, tile).into()
                })
                .collect(),
        }
    }

    pub fn new_with_tile_region(
        region: Region,
        tiles: &Tiles,
    ) -> Result<Self, grid::ReadOutOfBoundsError> {
        let coords: Vec<_> = region.iter_coords().collect();
        let tiles = tiles.grid.map_to_tiles(&coords)?;

        Ok(Self {
            operations: tiles
                .into_iter()
                .map(|(coord, entry)| {
                    let tile = entry.map(|(tile, _)| tile);
                    (coord, tile).into()
                })
                .collect(),
        })
    }

    pub fn combine_with(self, other: Self) -> Self {
        Self {
            operations: self
                .operations
                .into_iter()
                .chain(other.operations.into_iter())
                .collect(),
        }
    }
}

impl Default for TileStream {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum TileTemplate {
    Wall,
    Floor,
    ClosedDoor,
    OpenDoor,
}

impl From<TileTemplate> for Tile {
    fn from(id: TileTemplate) -> Self {
        match id {
            TileTemplate::Wall => Tile::new(0, true),
            TileTemplate::Floor => Tile::new(1, false),
            TileTemplate::ClosedDoor => Tile::new(2, true),
            TileTemplate::OpenDoor => Tile::new(3, false),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TilemapWriteOrders(pub TileStream);

impl TilemapWriteOrders {
    pub fn new() -> Self {
        Self(TileStream::new())
    }

    pub fn apply_stream(&mut self, stream: TileStream) {
        self.0.operations.extend(stream.operations.iter())
    }
}

impl Default for TilemapWriteOrders {
    fn default() -> Self {
        Self::new()
    }
}

pub fn tile_to_world(pos: UVec2) -> Vec2 {
    let convert = |v: u32| (v * TILE_SIZE) as f32;

    vec2(convert(pos.x), convert(pos.y))
}
